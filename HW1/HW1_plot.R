library("plotly")
data.labels = c(rep("Iterative fn",length(s)),rep("Recursive fn",length(s)),rep("Array fn",length(s)),rep("lfactorial fn",length(s)))
data = data.frame(N = s, Time = c(iterative.time,recursive.time,array.time,lfactorial.time), Labels=data.labels)
pal <- RColorBrewer::brewer.pal(nlevels(iris$Species), "Set1")
f <- list(
  family = "Bookman Old Style",
  size = 18,
  color = "#7f7f7f"
)
yax <- list(
  zeroline = TRUE,
  showline = TRUE,
  title = "Time (seconds)",
  mirror = "ticks",
  gridcolor = toRGB("gray50"),
  gridwidth = 1,
  linecolor = toRGB("black"),
  linewidth = 1,
  range = c(0,20)
)
xax <- list(
  zeroline = TRUE,
  showline = TRUE,
  mirror = "ticks",
  gridcolor = toRGB("gray50"),
  gridwidth = 1,
  linecolor = toRGB("black"),
  linewidth = 1,
  range = c(0,3800),
  tickvals = seq(from=0,to=3800,by=380)
)
p = plot_ly(data = data,x = N, y = Time,mode="markers", color=Labels,colors=pal)
layout(font=f,xaxis=xax,yaxis=yax)
write.csv(data,file="output.csv")