setwd("D:\\Users\\212415648\\Documents\\Masters\\Data Visualization and Analysis\\Project 2")
load("movies_merged")

# Determine number of observations that are not movies
length(which(movies_merged$Type != "movie"))
#  Remove them from the dataframe
movies_merged = movies_merged[which(movies_merged$Type == "movie"),]
nrow(movies_merged)
# Replace "N/A" with R's NA
dataframe.aslist <- lapply(movies_merged,gsub,pattern="N/A",replacement=NA)
# Coerce dataframe structure
movies_merged = data.frame(
  Title              = as.array(dataframe.aslist$Title),
  Year               = as.numeric(dataframe.aslist$Year),
  Rated              = as.factor(dataframe.aslist$Rated),
  Released           = as.Date(dataframe.aslist$Released),
  Runtime            = as.array(dataframe.aslist$Runtime),
  Genre              = as.array(dataframe.aslist$Genre),
  Director           = as.array(dataframe.aslist$Director),
  Writer             = as.array(dataframe.aslist$Writer),
  Actors             = as.array(dataframe.aslist$Actors),
  Plot               = as.array(dataframe.aslist$Plot),
  Language           = as.array(dataframe.aslist$Language),
  Country            = as.array(dataframe.aslist$Country),
  Awards             = as.array(dataframe.aslist$Awards),
  Metascore          = as.numeric(dataframe.aslist$Metascore),
  imdbRating         = as.numeric(dataframe.aslist$imdbRating),
  imdbVotes          = as.numeric(dataframe.aslist$imdbVotes),
  tomatoMeter        = as.numeric(dataframe.aslist$tomatoMeter),
  tomatoImage        = as.factor(dataframe.aslist$tomatoImage),
  tomatoRating       = as.numeric(dataframe.aslist$tomatoRating),
  tomatoReviews      = as.numeric(dataframe.aslist$tomatoReviews),
  tomatoFresh        = as.numeric(dataframe.aslist$tomatoFresh),
  tomatoRotten       = as.numeric(dataframe.aslist$tomatoRotten),
  tomatoConsensus    = as.array(dataframe.aslist$tomatoConsensus),
  tomatoUserMeter    = as.numeric(dataframe.aslist$tomatoUserMeter),
  tomatoUserRating   = as.numeric(dataframe.aslist$tomatoUserRating),
  tomatoUserReviews  = as.numeric(dataframe.aslist$tomatoUserReviews),
  DVD                = as.Date(dataframe.aslist$DVD),
  BoxOffice          = as.array((dataframe.aslist$BoxOffice)),
  Production         = as.character(dataframe.aslist$Production),
  Budget             = as.numeric(dataframe.aslist$Budget),
  Domestic_Gross     = as.numeric(dataframe.aslist$Domestic_Gross),
  Gross              = as.numeric(dataframe.aslist$Gross),
  Date               = as.numeric(dataframe.aslist$Date)
)
rm(dataframe.aslist)
# Remove unused columns
movies_merged$tomatoConsensus = NULL
movies_merged$Plot = NULL
movies_merged$Date = NULL
movies_merged$BoxOffice = NULL
movies_merged$DVD = NULL
# Remove films prior to 2000 and posterior to 2016
movies_merged = movies_merged[which(movies_merged$Year >= 2000 & movies_merged$Year <= 2016),]
# Remove all rows without gross or budget
movies_merged = movies_merged[which(!is.na(movies_merged$Gross) & !is.na(movies_merged$Budget)),]
# Extract release day, month and year from release date
movies_merged$Day = as.numeric(format(movies_merged$Released,"%d"))
movies_merged$Month = as.numeric(format(movies_merged$Released,"%m"))
# For the year, use the release year if available, if not keep the original value from the year variable
movies_merged$Year[which(!is.na(movies_merged$Released))] = as.numeric(format(movies_merged$Released[which(!is.na(movies_merged$Released))],"%Y"))
# Remove the released variable as it is no longer of use
movies_merged$Released = NULL

# Convert runtime to numeric
movies_merged$Runtime = as.numeric(lapply(movies_merged$Runtime, function(x) {
  time = NA
  if (length(grep("min",x)) != 0){
    if (length(grep("h",x)) != 0){
      pos = gregexpr("h",x)[[1]][1]
      hrs = as.numeric(substr(x,1,pos-1)) * 60
      x = substr(x,pos+1,nchar(x))
    } else {
      hrs = 0
    }
    pos = gregexpr("m",x)[[1]][1]
    min = as.numeric(substr(x,1,pos-1))
    time = hrs + min
  }
  return(time)
}))


# Extract number of wins and nominations for different categories
# Remove punctuation
movies_merged$Awards = gsub("[^[:alnum:][:space:]']", "", movies_merged$Awards)
# Convert everything to lower case
movies_merged$Awards = gsub(pattern = '([[:upper:]])', perl = TRUE, replacement = '\\L\\1', movies_merged$Awards)

library(stringr)

awards.split = strsplit(movies_merged$Awards,split = " ")
awards.numeric = lapply(awards.split,as.numeric)
movies_merged$Awards.sum = as.numeric(lapply(awards.numeric, function(x){
  return(sum(x[which(!is.na(x))]))
}))

# Remove awards variable
# movies_merged$Awards = NULL

# Calculate the profit
movies_merged$Profit = movies_merged$Gross - movies_merged$Budget
# Drop the Gross variables as they directly predict the profit
movies_merged$Domestic_Gross = NULL
movies_merged$Gross = NULL

# Get all the numeric variables from the dataframe
ColumnClass = cbind(sapply(movies_merged, class))
ColumnNumeric = which(ColumnClass == "numeric")
categoricalModel = movies_merged[,-c(ColumnNumeric)]
categoricalModel$Profit = movies_merged$Profit

trainingSplits = seq(0.05,0.95,by=0.05)

genres.split = lapply(strsplit(movies_merged$Genre,split=","),str_trim)
unique.genres = str_trim(sort(unique(abind(genres.split))))
Genre.binary = (lapply(genres.split,function (x){
  as.numeric(unique.genres %in% x)
}))
genres.matrix = do.call(rbind,Genre.binary)
colnames(genres.matrix) = paste("is.genre.",unique.genres,sep="")
row.names(genres.matrix) = categoricalModel$Title

categoricalModel = cbind(categoricalModel,genres.matrix)
categoricalModel$Genre = NULL

directors.split = lapply(strsplit(categoricalModel$Director,split=","),str_trim)
unique.directors = str_trim(sort(unique(abind(directors.split))))
directors.frequency = cbind(sort(table(abind(directors.split)),decreasing = TRUE))
directors.top200 = names(directors.frequency[1:200,])
Director.binary = (lapply(directors.split,function (x){
  as.numeric(directors.top200 %in% x)
}))
directors.matrix = do.call(rbind,Director.binary)
directors.matrix = cbind(directors.matrix,as.numeric(rowSums(directors.matrix) == 0))

directors.top200 = gsub("[^[:alnum:][:space:]']", "", directors.top200)
directors.top200 = paste("is.director.",str_replace(directors.top200," ",""),sep="")

colnames(directors.matrix) = c(directors.top200,"is.director Other")
row.names(directors.matrix) = categoricalModel$Title

categoricalModel = cbind(categoricalModel,directors.matrix)
categoricalModel$Director = NULL

writers.split = lapply(strsplit(categoricalModel$Writer,split=","),str_trim)
writers.split = lapply(writers.split,function(x){
  return(gsub("\\s*\\([^\\)]+\\)","",x))
})
unique.writers = str_trim(sort(unique(abind(writers.split))))
writers.frequency = cbind(sort(table(abind(writers.split)),decreasing = TRUE))
writers.top200 = names(writers.frequency[1:200,])
Writer.binary = (lapply(writers.split,function (x){
  as.numeric(writers.top200 %in% x)
}))
writers.matrix = do.call(rbind,Writer.binary)
writers.matrix = cbind(writers.matrix,as.numeric(rowSums(writers.matrix) == 0))

writers.top200 = gsub("[^[:alnum:][:space:]']", "", writers.top200)
writers.top200 = paste("is.writer.",str_replace(writers.top200," ",""),sep="")

colnames(writers.matrix) = c(writers.top200,"is.writer.Other")
row.names(writers.matrix) = categoricalModel$Title

categoricalModel = cbind(categoricalModel,writers.matrix)
categoricalModel$Writer = NULL


languages.split = lapply(strsplit(categoricalModel$Language,split=","),str_trim)
languages.split = lapply(languages.split,function(x){
  return(gsub("\\s*\\([^\\)]+\\)","",x))
})
unique.languages = str_trim(sort(unique(abind(languages.split))))
languages.frequency = cbind(sort(table(abind(languages.split)),decreasing = TRUE))
languages.top10 = names(languages.frequency[1:10,])
languages.binary = (lapply(languages.split,function (x){
  as.numeric(languages.top10 %in% x)
}))
languages.matrix = do.call(rbind,languages.binary)
languages.matrix = cbind(languages.matrix,as.numeric(rowSums(languages.matrix) == 0))

languages.top10 = gsub("[^[:alnum:][:space:]']", "", languages.top10)
languages.top10 = paste("is.language.",str_replace(languages.top10," ",""),sep="")

colnames(languages.matrix) = c(languages.top10,"is.language.Other")
row.names(languages.matrix) = categoricalModel$Title

categoricalModel = cbind(categoricalModel,languages.matrix)
categoricalModel$Language = NULL

actors.split = lapply(strsplit(categoricalModel$Actors,split=","),str_trim)
actors.split = lapply(actors.split,function(x){
  return(gsub("\\s*\\([^\\)]+\\)","",x))
})
unique.actors = str_trim(sort(unique(abind(actors.split))))
actors.frequency = cbind(sort(table(abind(actors.split)),decreasing = TRUE))
actors.top200 = names(actors.frequency[1:200,])
Actors.binary = (lapply(actors.split,function (x){
  as.numeric(actors.top200 %in% x)
}))
actors.matrix = do.call(rbind,Actors.binary)
actors.matrix = cbind(actors.matrix,as.numeric(rowSums(actors.matrix) == 0))

actors.top200 = gsub("[^[:alnum:][:space:]']", "", actors.top200)
actors.top200 = paste("is.actor.",str_replace(actors.top200," ",""),sep="")

colnames(actors.matrix) = c(actors.top200,"is.actor.Other")
row.names(actors.matrix) = categoricalModel$Title

categoricalModel = cbind(categoricalModel,actors.matrix)
categoricalModel$Actors = NULL

countries.split = lapply(strsplit(categoricalModel$Country,split=","),str_trim)
countries.split = lapply(countries.split,function(x){
  return(gsub("\\s*\\([^\\)]+\\)","",x))
})
unique.countries = str_trim(sort(unique(abind(countries.split))))
countries.frequency = cbind(sort(table(abind(countries.split)),decreasing = TRUE))
countries.top50 = names(countries.frequency[1:50,])
countries.binary = (lapply(countries.split,function (x){
  as.numeric(countries.top50 %in% x)
}))
countries.matrix = do.call(rbind,countries.binary)
countries.matrix = cbind(countries.matrix,as.numeric(rowSums(countries.matrix) == 0))

countries.top50 = gsub("[^[:alnum:][:space:]']", "", countries.top50)
countries.top50 = paste("is.country.",str_replace(countries.top50," ",""),sep="")

colnames(countries.matrix) = c(countries.top50,"is.country.Other")
row.names(countries.matrix) = categoricalModel$Title

categoricalModel = cbind(categoricalModel,countries.matrix)
categoricalModel$Country = NULL

productions.split = lapply(strsplit(as.character(categoricalModel$Production),split=","),str_trim)
productions.split = lapply(productions.split,function(x){
  return(gsub("\\s*\\([^\\)]+\\)","",x))
})
unique.productions = str_trim(sort(unique(abind(productions.split))))
productions.frequency = cbind(sort(table(abind(productions.split)),decreasing = TRUE))
productions.top100 = names(productions.frequency[1:100,])
productions.binary = (lapply(productions.split,function (x){
  as.numeric(productions.top100 %in% x)
}))
productions.matrix = do.call(rbind,productions.binary)
productions.matrix = cbind(productions.matrix,as.numeric(rowSums(productions.matrix) == 0))

productions.top100 = gsub("[^[:alnum:][:space:]']", "", productions.top100)
productions.top100 = paste("is.production.",str_replace(productions.top100," ",""),sep="")

colnames(productions.matrix) = c(productions.top100,"is.production.Other")
row.names(productions.matrix) = categoricalModel$Title

categoricalModel = cbind(categoricalModel,productions.matrix)
categoricalModel$Production = NULL

categoricalModel$Rated[which(is.na(categoricalModel$Rated) | categoricalModel$Rated == "NOT RATED")] = "UNRATED"
categoricalModel$Rated = as.character(categoricalModel$Rated)
ratings.split = lapply(strsplit(categoricalModel$Rated,split=","),str_trim)

unique.ratings = str_trim(sort(unique(abind(ratings.split))))
ratings.frequency = cbind(sort(table(abind(ratings.split)),decreasing = TRUE))

ratings.binary = (lapply(ratings.split,function (x){
  as.numeric(unique.ratings%in% x)
}))
ratings.matrix = do.call(rbind,ratings.binary)

ratings = paste("is.rating.",str_replace(unique.ratings," ",""),sep="")

colnames(ratings.matrix) = ratings
row.names(ratings.matrix) = categoricalModel$Title

categoricalModel = cbind(categoricalModel,ratings.matrix)
categoricalModel$Rated = NULL

########
categoricalModel$Awards = NULL
categoricalModel$Title = NULL
########

library(MASS)
categoricalModel$tomatoImage = as.character(categoricalModel$tomatoImage)
categoricalModel$tomatoImage[which(is.na(categoricalModel$tomatoImage))] = "none"

tomatoImages.split = lapply(strsplit(categoricalModel$tomatoImage,split=","),str_trim)
tomatoImages.split = lapply(tomatoImages.split,function(x){
  return(gsub("\\s*\\([^\\)]+\\)","",x))
})
unique.tomatoImages = str_trim(sort(unique(abind(tomatoImages.split))))
tomatoImages.frequency = cbind(sort(table(abind(tomatoImages.split)),decreasing = TRUE))

tomatoImages.binary = (lapply(tomatoImages.split,function (x){
  as.numeric(unique.tomatoImages%in% x)
}))
tomatoImages.matrix = do.call(rbind,tomatoImages.binary)
tomatoImages = paste("is.tomatoImage.",str_replace(unique.tomatoImages," ",""),sep="")

colnames(tomatoImages.matrix) = tomatoImages
row.names(tomatoImages.matrix) = categoricalModel$Title

categoricalModel = cbind(categoricalModel,tomatoImages.matrix)
categoricalModel$tomatoImage = NULL

trainingMSE_all = rep(0,length(trainingSplits))
validationMSE_all = rep(0,length(trainingSplits))

for(j in 1:length(trainingSplits)){
  trainingMSE = rep(0,10)
  validationMSE = rep(0,10)
  for(i in 1:10){
    trainingIndex = sample(nrow(categoricalModel),floor(trainingSplits[j]*nrow(categoricalModel)))
    model = lm(Profit ~ .,data=categoricalModel[trainingIndex,])
    #model = stepAIC(model, direction="both", trace = FALSE)
    trainingMSE[i] = mean(model$residuals^2)
    validationResponse = predict(model,newdata=categoricalModel[-trainingIndex,])
    validationMSE[i] = mean((categoricalModel[-trainingIndex,"Profit"]-validationResponse)^2)
  }
  trainingMSE_all[j] = mean(trainingMSE)
  validationMSE_all[j] = mean(validationMSE)
}

plot1 = ggplot() + 
  geom_line(aes(x = trainingSplits, y = trainingMSE_all),color = "purple") +
  geom_line(aes(x = trainingSplits, y = validationMSE_all),color = "purple",linetype = 2) + 
  scale_x_continuous(expand = c(0,0), limits = c(0.25,0.95), breaks = seq(0.25,0.95,by=0.05)) +
  scale_y_continuous(expand = c(0,0), limits = c(1e10,3e16))
print(plot1)